# "Everything else" of crates.rs project

It's a crate that ties multiple data sources together in the crates.rs project.

This is entirely for internal use in the crates.rs project, so see [crates.rs general documentation for more information](https://gitlab.com/crates.rs/crates.rs).
